<?php

class ChallengeController extends BaseController {
	public function get($battle) {
        $challenge = Challenge::where('battle', '=', $battle)->first();

        if (!$challenge) {
            return App::abort(404);
        }

        return View::make('challenge', array('challenge' => $challenge));
	}

}
