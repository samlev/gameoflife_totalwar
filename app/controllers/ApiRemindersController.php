<?php

class ApiRemindersController extends RemindersController {

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
        $user = User::where('username', '=', strtolower(Input::get('username')))->first();
        if (!$user) {
            return Response::json(array('error' => 'User does not exist'), 404);
        }
        if (empty($user->email)) {
            return Response::json(array('error' => 'User has no email address'), 401);
        }

		switch ($response = Password::remind(array('username'=>$user->username), function($message) {
            $message->subject('Password Reminder');
        }))
		{
			case Password::INVALID_USER:
				return Response::json(array('error' => Lang::get($response)), 401);

			case Password::REMINDER_SENT:
				return Response::json(array('status' => Lang::get($response)));
		}
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		$credentials = Input::only(
			'username', 'password', 'password_confirmation', 'token'
		);
        $credentials['username'] = strtolower($credentials['username']);

        Password::validator(function($credentials) {
            return strlen($credentials['password']) >= 8;
        });

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);

			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Response::json(array('error' =>  Lang::get($response), 403));

			case Password::PASSWORD_RESET:
				return Response::json(array('status' => Lang::get($response)));
		}
	}

}
