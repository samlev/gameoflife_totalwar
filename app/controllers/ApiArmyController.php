<?php

class ApiArmyController extends BaseController {
	public function get($id) {
        $army = Army::find($id);

        if ($army) {
            return Response::json($army);
        }

        return Response::json(array('error' => 'Cannot find army'), 404);
	}

    public function update($id) {
        $army = Army::find($id);

        if ($army) {
            if ($army->owner != Auth::user()->id) {
                return Response::json(array('error' => 'You do not own this army'), 403);
            }
            Input::merge(array_map('trim', Input::all()));

            $validator = Validator::make(
                Input::all(),
                array(
                    'commander' => 'required',
                )
            );

            if ($validator->fails()) {
                return Response::json(array('error' => 'Army must have a commander'), 403);
            }

            $army->commander = Input::get('commander');
            $army->active = (Input::has('active') ? 1 : 0);
            $army->save();

            if ($army->active === 0 && Session::get('army_id', null) == $army->id) {
                Session::forget('army_id');
            }

            return Response::json($army);
        }

        return Response::json(array('error' => 'Cannot find army'), 404);
    }

    public function current() {
        $id = Session::get('army_id');

        $army = Army::find($id);

        if ($army) {
            if ($army->active === 0) {
                Session::forget('army_id');
                return Response::json(array('error' => 'This army is not active'), 403);
            }

            return Response::json($army);
        }

        return Response::json(array('error' => 'Cannot find army'), 404);
    }

    public function create() {
        if (Auth::check() || Session::get('army_id', null) === null) {
            Input::merge(array_map('trim', Input::all()));

            $validator = Validator::make(
                Input::all(),
                array(
                    'name' => 'required|unique:armies,name',
                    'commander' => 'required',
                )
            );

            if ($validator->fails()) {
                return Response::json(array('error' => 'Cannot create army', 'errors' => $validator->messages()->toArray()), 403);
            }

            $army = new Army();
            $army->name = Input::get('name');
            $army->commander = Input::get('commander');
            $army->owner = (Auth::check() ? Auth::user()->id : null);
            $army->strength = 100;
            $army->victories = 0;
            $army->losses = 0;
            $army->active = 1;
            $army->save();

            Session::put('army_id', $army->id);

            return Response::json($army);
        }

        return Response::json(array('error' => 'Cannot create army. Please log in or register to create a new army'), 401);
    }

    public function select($id) {
        $army = Army::find($id);

        if ($army && $army->active == 1) {
            if (Auth::check() && $army->owner == Auth::user()->id) {
                Session::put('army_id', $army->id);

                return Response::json($army);
            } else {
                return Response::json(array('error' => 'You cannot select that army. It is not yours.'), 403);
            }
        } else {
            return Response::json(array('error' => 'Army does not exist, or has been disbanded'), 404);
        }
    }

}
