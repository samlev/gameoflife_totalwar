<?php

class ApiUserController extends BaseController {
	public function register() {
        Input::merge(array_map('trim', array('username' => Input::get('username'))));

        $validator = Validator::make(
            array(
                'username' => strtolower(Input::get('username')),
                'email' => Input::get('email'),
                'password' => Input::get('password'),
                'confirm' => Input::get('confirm'),
            ),
            array(
                'username' => 'required|unique:users,username',
                'email' => 'email',
                'password' => 'required|min:8',
                'confirm' => 'required|same:password',
            ),
            array(
                'confirm' => 'Passwords do not match'
            )
        );

        if ($validator->fails()) {
            return Response::json(array('error' => 'Cannot create account', 'errors' => $validator->messages()->toArray()), 403);
        }

        $user = new User();

        $user->username = strtolower(Input::get('username'));
        $user->display = Input::get('username');
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));

        $user->save();

        // now log the user in
        return App::make('ApiAuthController')->login();
	}
}
