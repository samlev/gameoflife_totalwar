<?php

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class ApiChallengeController extends BaseController {
	public function get($battle) {
        $challenge = Challenge::where('battle', '=', $battle)->first();

        if ($challenge) {
            return Response::json($challenge);
        }

        return Response::json(array('error' => 'Cannot find challenge'), 404);
	}

    public function create() {
        if (Session::get('army_id', null) === null) {
            return Response::json(array('error' => 'You must create or select an army before you can challenge someone'), 403);
        }

        $army = Army::find(Session::get('army_id'));

        if (!$army || $army->ready() === false) {
            return Response::json(array('error' => 'That army has been disbanded or is already involved in a battle'), 403);
        }

        $battle = $this->random_id(mt_rand(5, 15));

        $validator = Validator::make(
            array('battle' => $battle),
            array(
                'name' => 'unique:challenges,battle',
            )
        );

        while ($validator->fails()) {
            $battle = $this->random_id(mt_rand(5, 15));

            $validator = Validator::make(
                array('battle' => $battle),
                array(
                    'name' => 'unique:challenges,battle',
                )
            );
        }

        $challenge = new Challenge();
        $challenge->battle = $battle;
        $challenge->challenger_id = $army->id;

        /**
         * Basic dimensions:
         *
         * 50-400 generations
         * 80-200 total board width
         * 60-150 total board height
         *
         * "Command width" is approximately 45% of the board width
         */
        $challenge->generations = $this->clamp(intval(Input::get('generations', 0)), 50, 400);
        $challenge->width = $this->clamp(intval(Input::get('width', 0)), 80, 200);
        $challenge->height = $this->clamp(intval(Input::get('height', 0)), 60, 150);
        $challenge->command_width = ceil(($challenge->width / 5) * 2.25);
        $challenge->challenger_final = 0;
        $challenge->defender_final = 0;

        $civilian_width = ceil($challenge->width / 3);

        // build a blank board
        $board = $this->inflate_board(null, $challenge->width, $challenge->height);

        // populate with 'civilians', favouring the centre
        foreach ($board as $y => &$row) {
            foreach ($row as $x => &$v) {
                if ($x < $civilian_width || $x > ($challenge->width - $civilian_width)) {
                    if ($x > $civilian_width) {
                        $x = (($x * -1) + ceil($challenge->width / 2));
                    }

                    // least populated areas are the edge quarters, most populated is the middle 1/5
                    if ($x < ($challenge->width / 4)) {
                        if (mt_rand(0, 20) == 0) {
                            $v = 1;
                        }
                    } else {
                        if (mt_rand(0, 12) == 0) {
                            $v = 1;
                        }
                    }
                } else {
                    if (mt_rand(0, 7) == 0) {
                        $v = 1;
                    }
                }
            }
        }

        // run one generation co 'clean up'
        $board = $this->generation($board);

        $challenge->board_start = json_encode($this->deflate_board($board));

        $challenge->save();

        // rate limit for bots/heavy users
        $this->rate_limit();

        return Response::json($challenge);
    }

    public function accept($battle) {
        $challenge = Challenge::where('battle', '=', $battle)->first();

        if (!$challenge) {
            return Response::json(array('error' => 'Cannot find challenge'), 404);
        }

        if ($challenge->defender_id !== null) {
            return Response::json(array('error' => 'Challenge has already been accepted'), 403);
        }

        if (Session::get('army_id', null) === null) {
            return Response::json(array('error' => 'You must create or select an army before you can accept a challenge'), 403);
        }

        $army = Army::find(Session::get('army_id'));

        if (!$army || $army->ready() === false) {
            // note: this effectively means that an army can't challenge itself, because it would already be involved in a battle
            return Response::json(array('error' => 'That army has been disbanded or is already involved in a battle'), 403);
        }

        $challenger = Army::find($challenge->challenger_id);

        if ($challenger->owner !== null && $army->owner !== null) {
            if ($challenger->owner == $army->owner) {
                return Response::json(array('error' => 'You cannot challenge your own armies'), 403);
            }
        }

        $challenge->defender_id = $army->id;
        $challenge->save();

        // rate limit for bots/heavy users
        $this->rate_limit();

        return Response::json($challenge);
    }

    public function place($battle) {
        $challenge = Challenge::where('battle', '=', $battle)->first();

        if (!$challenge) {
            return Response::json(array('error' => 'Cannot find challenge'), 404);
        }

        if ($challenge->final_generation !== null) {
            return Response::json(array('error' => 'Battle has already been completed'), 403);
        }

        if (Session::get('army_id', null) === null) {
            return Response::json(array('error' => 'You must select an army to place pieces'), 403);
        }

        $army = Army::find(Session::get('army_id'));

        if (!$army || $army->active == 0) {
            return Response::json(array('error' => 'You must select an active army to place pieces'), 403);
        }

        if ($challenge->challenger_id == $army->id) {
            $my_num = 2;
            $x_min = 0;
            $x_max = $challenge->command_width - 1;

            if ($challenge->challenger_complete !== null) {
                return Response::json(array('error' => 'Your time for placing pieces has run out'), 403);
            }
        } else if ($challenge->defender_id == $army->id) {
            $my_num = 3;
            $x_max = $challenge->width - 1;
            $x_min = $x_max - $challenge->command_width;

            if ($challenge->defender_complete !== null) {
                return Response::json(array('error' => 'Your time for placing pieces has run out'), 403);
            }
        } else {
            return Response::json(array('error' => 'Your army isn\'t involved in this battle'), 403);
        }

        $points_left = $army->strength;
        $board_civilian = $this->inflate_board(json_decode($challenge->board_start, true), $challenge->width, $challenge->height);
        $my_board = $this->inflate_board(null, $challenge->width, $challenge->height);

        $placements = json_decode(Input::get('placements', '[]'), true);

        foreach ($placements as $p) {
            $x = $this->clamp($p['x'], $x_min, $x_max);
            $y = $this->clamp($p['y'], 0, $challenge->height - 1);

            if ($board_civilian[$y][$x] == 1) {
                continue;
            } else if ($my_board[$y][$x] == 0) {
                $my_board[$y][$x] = $my_num;
                $points_left --;
            }

            if ($points_left == 0) {
                break;
            }
        }

        if ($points_left == $army->strength) {
            return Response::json(array('error' => 'Your army cannot send no-one into battle'), 403);
        }

        if ($my_num == 2) {
            $challenge->challenger_start = json_encode($this->deflate_board($my_board));
            $challenge->challenger_complete = new DateTime();
        } else {
            $challenge->defender_start = json_encode($this->deflate_board($my_board));
            $challenge->defender_complete = new DateTime();
        }

        $challenge->save();

        return Response::json($challenge);
    }

    public function ready($battle) {
        $challenge = Challenge::where('battle', '=', $battle)->first();

        if (!$challenge) {
            return Response::json(array('error' => 'Cannot find challenge'), 404);
        }

        if ($challenge->ready !== null) {
            return Response::json($challenge);
        }

        if ($challenge->challenger_complete === null || $challenge->defender_complete === null) {
            return Response::json(array('error' => 'Battle cannot be made ready until both teams have placed their pieces'), 403);
        }

        $board_civilian = $this->inflate_board(json_decode($challenge->board_start, true), $challenge->width, $challenge->height);
        $board_challenger = $this->inflate_board(json_decode($challenge->challenger_start, true), $challenge->width, $challenge->height);
        $board_defender = $this->inflate_board(json_decode($challenge->defender_start, true), $challenge->width, $challenge->height);

        $board_complete = $this->combine_boards($this->combine_boards($board_civilian, $board_challenger), $board_defender);

        $challenge->board_start = json_encode($this->deflate_board($board_complete));
        $challenge->ready = new DateTime();

        $challenge->save();

        $c = new Celery(Config::get('rabbitmq.host'),
                        Config::get('rabbitmq.user'),
                        Config::get('rabbitmq.pass'),
                        Config::get('rabbitmq.vhost'),
                        Config::get('rabbitmq.queue'));

        $result = $c->PostTask('gameoflife_totalwar.tasks.process_task', array($challenge->id), false, Config::get('rabbitmq.queue'));

        return Response::json($challenge);
    }

    protected function random_id($length = 20) {
        $str = '';
        $source = implode('', range('a', 'z')) . implode('', range('A', 'Z')) . implode('', range(0, 9)) . '-_~';

        while (strlen($str) < $length) {
            $str .= $source[mt_rand(0, strlen($source)-1)];
        }

        return $str;
    }

    protected function clamp($num, $min, $max) {
        return max($min, min($num, $max));
    }

    protected function inflate_board($points = null, $w, $h) {
        $b = array();

        for ($y = 0; $y < $h; $y ++) {
            $b[] = array();
            for ($x = 0; $x < $w; $x ++) {
                $b[$y][] = ($points !== null && isset($points[$y][$x]) ? $points[$y][$x] : 0);
            }
        }

        return $b;
    }

    protected function deflate_board($board) {
        $points = array();

        foreach ($board as $y => $row) {
            foreach ($row as $x => $v) {
                if ($v != 0) {
                    if (!isset($points[$y])) {
                        $points[$y] = array();
                    }

                    $points[$y][$x] = $v;
                }
            }
        }

        return $points;
    }

    protected function count_alive($board) {
        // 1 = civilian, 2 = attacker, 3 = defender
        $count = array(1 => 0, 2 => 0, 3 => 0, 'total' => 0);

        foreach ($board as $y => $row) {
            foreach ($row as $x => $v) {
                if ($v > 0) {
                    $count[$v] ++;
                    $count['total'] ++;
                }
            }
        }

        return $count;
    }

    protected function combine_boards($board1, $board2) {
        $board = array();

        foreach ($board1 as $y => $row) {
            $board[$y] = array();
            foreach ($row as $x => $v) {
                $board[$y][$x] = ($v > 0 ? $v : ($board2[$y][$x] > 0 ? $board2[$y][$x] : 0));
            }
        }

        return $board;
    }

    protected function generation($board) {
        $b2 = array();

        foreach ($board as $y => $row) {
            $b2[] = array();
            foreach ($row as $x => $v) {
                $b2[$y][] = $this->alive($y, $x, $board);
            }
        }

        return $b2;
    }

    protected function alive($y, $x, $b) {
        $neighbors = 0;
        $neighbors_2 = 0;
        $neighbors_3 = 0;

        foreach (range($this->clamp($y-1, 0, $y), $this->clamp($y+1, $y, count($b)-1)) as $i) {
            foreach (range($this->clamp($x-1, 0, $x), $this->clamp($x+1, $x, count($b[$y])-1)) as $j) {
                if ($i == $y && $j == $x) {
                    continue;
                }

                if ($b[$i][$j] > 0) {
                    $neighbors ++;
                    if ($b[$i][$j] == 2) {
                        $neighbors_2 ++;
                    } else if ($b[$i][$j] == 3) {
                        $neighbors_3 ++;
                    }
                }
            }
        }

        if ($b[$y][$x] != 0) {
            if ($neighbors > 3 || $neighbors < 2) {
                return 0;
            }
        } else {
            if ($neighbors == 3) {
                if ($neighbors_2 >= 2) {
                    return 2;
                } else if ($neighbors_3 >= 2) {
                    return 3;
                }

                return 1;
            }
        }

        return $b[$y][$x];
    }

    public function rate_limit() {
        if (Session::get('rate_limit', null) === null) {
            $army = Army::find(Session::get('army_id'));

            if ($army) {
                if (Session::get('rate_limit', null) === null) {
                    if ($army->games_per_hour() > 10) {
                        Session::set('rate_limit', true);
                    } else if (Auth::check()) {
                        if (Auth::user()->games_per_hour() > 30) {
                            Session::set('rate_limit', true);
                        }
                    }
                }
            } else if (Auth::check()) {
                if (Auth::user()->games_per_hour() > 30) {
                    Session::set('rate_limit', true);
                }
            }
        }
    }
}
