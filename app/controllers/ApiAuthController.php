<?php

class ApiAuthController extends BaseController {
	public function login() {
        $user = array(
            'username' => strtolower(Input::get('username')),
            'password' => Input::get('password')
        );

        if (Auth::attempt($user, Input::has('remember'))) {
            if (Session::get('army_id', null) !== null) {
                $army = Army::find(Session::get('army_id'));
                if ($army) {
                    if ($army->owner === null) {
                        $army->owner = Auth::user()->id;
                        $army->save();
                    } else {
                        if ($army->owner != Auth::user()->id) {
                            Session::forget('army_id');
                        }
                    }
                } else {
                    Session::forget('army_id');
                }
            }

            App::make('ApiChallengeController')->rate_limit();

            return Response::json(array('user' => Auth::user(), 'csrf_token' => csrf_token()));
        }

        return Response::json(array('error' => 'Username or password incorrect'), 401);
	}

    public function logout() {
        Auth::logout();
        Session::flush();
        Session::regenerate();

        return Response::json(array('info' => 'Successfully logged out'));
    }

}
