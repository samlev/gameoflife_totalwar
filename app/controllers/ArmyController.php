<?php

class ArmyController extends BaseController {
	public function get($id) {
        $army = Army::find($id);

        if (!$army) {
            return App::abort(404);
        }

        return View::make('army', array('army' => $army));
	}

}
