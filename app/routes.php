<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
	return View::make('home');
});
Route::get('how-to-play', function() {
    return View::make('how-to-play');
});
Route::get('about', function() {
    return View::make('about');
});
Route::get('technology', function() {
    return View::make('technology');
});
Route::get('privacy', function() {
    return View::make('privacy');
});

Route::get('password-reset/{token?}', array('as' => 'password-reset', 'uses' => 'RemindersController@getReset'))->before('guest');

Route::group(array('prefix' => 'api'), function() {
    Route::group(array('prefix' => 'v1'), function() {
        Route::group(array('prefix' => 'army'), function() {
            Route::get('/', array('as' => 'api/v1/army/current',
                                   'uses' => 'ApiArmyController@current'));
            Route::post('/', array('as' => 'api/v1/army/create',
                                   'uses' => 'ApiArmyController@create'))->before('apicsrf');
            Route::get('{id?}', array('as' => 'api/v1/army/get',
                                     'uses' => 'ApiArmyController@get'));
            Route::post('{id?}', array('as' => 'api/v1/army/update',
                                       'uses' => 'ApiArmyController@update'))->before('auth')->before('apicsrf');
            Route::post('{id?}/select', array('as' => 'api/v1/army/select',
                                              'uses' => 'ApiArmyController@select'))->before('auth')->before('apicsrf');
        });

        Route::group(array('prefix' => 'challenge'), function() {
            Route::get('{battle?}', array('as' => 'api/v1/challenge/get',
                                          'uses' => 'ApiChallengeController@get'));
            if (Session::get('rate_limit', null) === null) {
                Route::post('/', array('as' => 'api/v1/challenge/create',
                                       'uses' => 'ApiChallengeController@create'))->before('apicsrf');
                Route::post('{battle?}/accept', array('as' => 'api/v1/challenge/accept',
                                                      'uses' => 'ApiChallengeController@accept'))->before('apicsrf');
            } else {
                Route::post('/', array('as' => 'api/v1/challenge/create',
                                       'uses' => 'ApiChallengeController@create'))->before('apicsrf')->before('apicaptcha');
                Route::post('{battle?}/accept', array('as' => 'api/v1/challenge/accept',
                                                      'uses' => 'ApiChallengeController@accept'))->before('apicsrf')->before('apicaptcha');
            }
            Route::post('{battle?}/place', array('as' => 'api/v1/challenge/place',
                                                 'uses' => 'ApiChallengeController@place'))->before('apicsrf');
            Route::post('{battle?}/ready', array('as' => 'api/v1/challenge/ready',
                                                 'uses' => 'ApiChallengeController@ready'))->before('apicsrf');
        });

        Route::post('register', array('as' => 'api/v1/register',
                                   'uses' => 'ApiUserController@register'))->before('apiguest')->before('apicsrf');
        Route::post('login', array('as' => 'api/v1/login',
                                   'uses' => 'ApiAuthController@login'))->before('apiguest')->before('apicsrf');
        Route::post('logout', array('as' => 'api/v1/logout',
                                    'uses' => 'ApiAuthController@logout'))->before('apiauth')->before('apicsrf');
        Route::post('forgot-password', array('as' => 'api/v1/forgot-password',
                                             'uses' => 'ApiRemindersController@postRemind'))->before('apiguest')->before('apicsrf');
        Route::post('reset-password', array('as' => 'api/v1/reset-password',
                                            'uses' => 'ApiRemindersController@postReset'))->before('apiguest')->before('apicsrf');
    });
});

Route::get('challenge/{battle?}', array('as' => 'challenge/get',
                                        'uses' => 'ChallengeController@get'));

Route::get('army/{id?}', array('as' => 'army/get',
                               'uses' => 'ArmyController@get'));