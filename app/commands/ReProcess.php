<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ReProcess extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'challenges:reprocess';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Re-adds items to the queue for processing';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $c = new Celery(Config::get('rabbitmq.host'),
                        Config::get('rabbitmq.user'),
                        Config::get('rabbitmq.pass'),
                        Config::get('rabbitmq.vhost'),
                        Config::get('rabbitmq.queue'));

        foreach (Challenge::whereNotNull('ready')->whereNull('final_generation')->get() as $challenge) {
            $result = $c->PostTask('gameoflife_totalwar.tasks.process_task', array($challenge->id), false, Config::get('rabbitmq.queue'));
            $this->info('Added challenge '.$challenge->id.' to the processing queue');
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
