<?php
/**
 * Army.php
 * 
 * @package
 * @author: Samuel Levy <sam@samuellevy.com>
 */

class Army extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'armies';

    public function owner() {
        return $this->belongsTo('User', 'owner');
    }

    public function ready() {
        if ($this->active == 0) {
            return false;
        }

        if ($this->current_challenge()) {
            return false;
        }

        return true;
    }

    public function current_challenge() {
        return Challenge::whereNull('final_generation')
                          ->where(function($query) {
                              $query->where('challenger_id', '=', $this->id)
                                    ->orWhere('defender_id', '=', $this->id);
                          })->first();
    }

    public function challenges() {
        return Challenge::where('challenger_id', '=', $this->id)
                          ->orWhere('defender_id', '=', $this->id);
    }

    public function past_challenges() {
        return Challenge::whereNotNull('final_generation')
                        ->where(function($query) {
                            $query->where('challenger_id', '=', $this->id)
                                ->orWhere('defender_id', '=', $this->id);
                        });
    }

    public function games_per_hour() {
        return Challenge::whereNotNull('ready')
            ->where('ready', '>=', new DateTime('-1 hour'))
            ->where(function($query) {
                $query->where('challenger_id', '=', $this->id)
                    ->orWhere('defender_id', '=', $this->id);
            })->count();
    }
}