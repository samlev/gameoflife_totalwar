<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    public function armies() {
        return $this->hasMany('Army', 'owner', 'id');
    }

    public function formations() {
        return $this->hasMany('Formation');
    }

    public function games_per_hour() {
        $games = 0;

        foreach ($this->armies()->get() as $a) {
            $games += $a->games_per_hour();
        }

        return $games;
    }

}
