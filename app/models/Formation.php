<?php
/**
 * Formation.php
 * 
 * @package
 * @author: Samuel Levy <sam@samuellevy.com>
 */

class Formation extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'formations';
}