<?php
/**
 * Challenge.php
 * 
 * @package
 * @author: Samuel Levy <sam@samuellevy.com>
 */

class Challenge extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'challenges';

    public function getDates() {
        return array('created_at', 'updated_at', 'challenger_complete', 'defender_complete', 'ready');
    }

    protected $hidden = array('challenger_start', 'defender_start');

    /**
     * Gets the challenging army
     *
     * @return Army The challenging army
     */
    public function challenger() {
        return $this->hasOne('Army', 'id', 'challenger_id');
    }

    /**
     * Gets the defending army if one has been set yet
     *
     * @return mixed The defending army or null
     */
    public function defender() {
        return $this->hasOne('Army', 'id', 'defender_id');
    }

    /**
     * Returns the winning army if the match has completed. A tie is a win for the defender.
     *
     * @return Army|null The winning army, or null if no winner has been decided
     */
    public function winner() {
        if ($this->final_generation !== null) {
            if ($this->challenger_final > $this->defender_final) {
                return $this->challenger();
            } else {
                return $this->defender();
            }
        }

        return null;
    }

    public function board() {
        return json_decode($this->board_start, true);
    }

    public function timeAgo() {
        $diff = $this->created_at->diff(new DateTime(), true);

        $format = 'a few milliseconds ago';
        if ($diff->y > 0) {
            $format = '%y year' . ($diff->y == 1 ? '' : 's') . ' ago';
        } else {
            if ($diff->m > 0) {
                $format = '%m month' . ($diff->m == 1 ? '' : 's') . ' ago';
            } else {
                if ($diff->d > 0) {
                    $format = '%d day' . ($diff->d == 1 ? '' : 's') . ' ago';
                } else {
                    if ($diff->h > 0) {
                        $format = '%h hour' . ($diff->h == 1 ? '' : 's') . ' ago';
                    } else {
                        if ($diff->i > 0) {
                            $format = '%i minute' . ($diff->i == 1 ? '' : 's') . ' ago';
                        } else {
                            if ($diff->s > 0) {
                                $format = '%s second' . ($diff->s == 1 ? '' : 's') . ' ago';
                            }
                        }
                    }
                }
            }
        }

        return $diff->format($format);
    }
}