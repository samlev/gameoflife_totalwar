<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('challenges', function($table)
        {
            $table->increments('id');
            $table->string('battle', 20)->unique();
            $table->integer('challenger_id');
            $table->integer('defender_id')->nullable();
            $table->integer('generations');
            $table->integer('width');
            $table->integer('height');
            $table->integer('command_width');
            $table->longText('challenger_start');
            $table->longText('defender_start');
            $table->longText('board_start');
            $table->timestamp('challenger_complete')->nullable();
            $table->timestamp('defender_complete')->nullable();
            $table->timestamp('ready')->nullable();
            $table->integer('final_generation')->nullable();
            $table->longText('board_end')->nullable();
            $table->integer('challenger_final')->nullable();
            $table->integer('defender_final')->nullable();
            $table->integer('civilian_final')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('challenges');
	}

}
