<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('formations', function($table)
        {
            $table->increments('id');
            $table->integer('owner');
            $table->string('title');
            $table->integer('cost');
            $table->integer('width');
            $table->integer('height');
            $table->text('formation');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('formations');
	}

}
