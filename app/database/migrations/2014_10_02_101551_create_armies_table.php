<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArmiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('armies', function($table)
        {
            $table->increments('id');
            $table->integer('owner')->nullable();
            $table->string('name');
            $table->string('commander');
            $table->integer('strength');
            $table->integer('victories');
            $table->integer('losses');
            $table->tinyInteger('active');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('armies');
	}

}
