@extends('__master')

@section('pagetitle')
Privacy
@stop

@section('pagecontent')
<div class="row">
<div class="lead">
<h2 class="section-heading">
Privacy
</h2>
  <p>
    I won't use your email address for marketing, and I won't hand it out to other entities/companies/whatever. If your
    email address gets used at all, it will be for forgotten password notifications.
  </p>
  <p>
    Even though I store passwords securely, there's always the possibility that someone will somehow break into the
    server and make off with the user database. In that unlikely event, the nefarious bastard may have even figured out
    how to crack bcrypt. In case that extremely unlikely event happens, you should probably pick a unique password for
    this site. If you have trouble keeping track of all of these unique passwords that you <em>really should</em> be
    using, maybe you can use <a href="https://lastpass.com/" target="_blank">LastPass</a> or
    <a href="http://keepass.info/" target="_blank">KeePass</a>.
  </p>
  <p>
    Cookies get used to keep track of you on the site, and I use Google Analytics, and Google AdSense.
  </p>
</div>
</div>
@stop