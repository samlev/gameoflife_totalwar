@extends('__master')

@section('pagetitle')
{{{$army->name}}} led by {{{$army->commander}}}
@stop

@section('pagecontent')
<h2>You are at the camp of {{{$army->name}}}</h2>
<p>
@if ($army->active == 1)
  {{{$army->name}}} is an active army, consisting of
  @if($army->strength <= 1)
    the lonely {{{$army->commander}}}, who has no more troops to command
  @else
    {{{$army->strength}}} warriors, led by the brave {{{$army->commander}}}
  @endif
@else
  {{{$army->name}}} was an army, who disbanded
  @if($army->strength <= 1)
    when the lonely {{{$army->commander}}}, had no more troops to command
  @else
    after {{{$army->strength}}} warriors decided to stop following {{{$army->commander}}}.
  @endif
@endif
</p>
<h3>Vital stats</h3>
<p>
  <strong>Victories:</strong> {{{$army->victories}}}<br>
  <strong>Defeats:</strong> {{{$army->losses}}}
</p>
@if ($challenge = $army->current_challenge())
  {{{$army->name}}} is currently
  @if ($challenge->challenger_id == $army->id)
     @if ($challenge->defender_id == null)
         <a href="/challenge/{{{$challenge->battle}}}">looking for a fight</a>.
     @else
         <a href="/challenge/{{{$challenge->battle}}}">attacking</a> an army called <a href="/army/{{{$challenge->defender->id}}}">{{{$challenge->defender->name}}}</a>
     @endif
  @else
    <a href="/challenge/{{{$challenge->battle}}}">defending</a> against <a href="/army/{{{$challenge->challenger->id}}}">{{{$challenge->challenger->name}}}</a>
  @endif
@endif
@if ($army->past_challenges()->count() > 0)
  @foreach($army->past_challenges()->get() as $c)
    <div>
     <a href="/army/{{{$c->challenger->id}}}">{{{$c->challenger->name}}}</a> attacked <a href="/army/{{{$c->defender->id}}}">{{{$c->defender->name}}}</a>,
     resulting in a
     @if ($c->challenger->id == $army->id)
       @if ($c->challenger_final > $c->defender_final)
         victory for {{{$army->name}}}, picking up 10 new recruits.
       @else
         loss for {{{$army->name}}}, losing 10 good men.
       @endif
     @else
       @if ($c->challenger_final <= $c->defender_final)
         victory for {{{$army->name}}}, picking up 5 new recruits.
       @else
         loss for {{{$army->name}}}, losing 5 good men.
       @endif
     @endif
     <a href="/challenge/{{{$c->battle}}}">See the battle</a>.
    </div>
  @endforeach
@else
<div>
{{{$army->name}}} has not been caught up in any previous conflicts
</div>
@endif
@stop
