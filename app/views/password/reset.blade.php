@extends('__master')

@section('pagetitle')
Reset your password
@stop

@section('pagecontent')
<div class="row">
<div class="lead">
<h2 class="section-heading">
Reset your password
</h2>
<form id="reset">
<div id="reset_errors"></div>
<input id="reset_token" type="hidden" value="{{{ $token }}}">
<span style="font-weight:bold;">User Name</span><br>
<input type="text" placeholder="User Name" id="reset_username" /><br><br>
<span>Password</span><br>
<input type="password" placeholder="Password" id="reset_password" /><br><br>
<span>Confirm Password</span><br>
<input type="password" placeholder="Re-type Password" id="reset_password_confirmation" /><br><br>
<input style="float: left" type="submit" class="btn btn-success"  value="Reset my password" />
</form>
</div>
</div>
@stop