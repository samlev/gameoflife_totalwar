@extends('__master')

@section('pagetitle')
Challenge!
@stop

@section('pagecontent')
<?php $myarmy = isset($myarmy) ? $myarmy : Army::find(Session::get('army_id')); ?>
<div class="col-xs-12 col-sm-12 col-md-7 col-lg-6">
<div>
@if ($challenge->ready !== null)
  @include('macros.board')
@elseif ($myarmy)
  @if ($myarmy->current_challenge() && $myarmy->current_challenge()->id == $challenge->id)
     @if (($challenge->challenger_complete !== null && $challenge->challenger->id == $myarmy->id) || ($challenge->defender_complete !== null && $challenge->defender && $challenge->defender->id == $myarmy->id))
       @include('macros.boardwait')
     @else
       @include('macros.boardplace')
     @endif
  @else
     @include('macros.boardwaitpublic')
  @endif
@else
  @include('macros.boardwaitpublic')
@endif
</div>
</div>
<div class="pull-right lead battleinfo col-xs-12 col-sm-12 col-md-4 col-lg-5">
  <p>
  @if ($challenge->width < 90)
      In the lobby of a hotel,
  @elseif ($challenge->width < 110)
      Among a couple of buildings,
  @elseif ($challenge->width < 140)
      Across an entire city block,
  @elseif ($challenge->width < 190)
      In a wide open field,
  @else
      Carrying their biggest weapons,
  @endif
  @if ($challenge->defender)
      {{{ $challenge->challenger->commander }}} led <a href="/army/{{{ $challenge->challenger->id }}}">{{{ $challenge->challenger->name }}}</a>
      @if ($challenge->generations < 75)
          into a short skirmish with
      @elseif ($challenge->generations < 175)
          into a territorial dispute with
      @elseif ($challenge->generations < 275)
          into a battle with
      @elseif ($challenge->generations < 375)
          as they laid a siege against
      @else
          as they waged war against
      @endif
      <a href="/army/{{{ $challenge->defender->id }}}">{{{ $challenge->defender->name }}}</a>, led by {{{ $challenge->defender->commander }}}
  @else
      {{{ $challenge->challenger->commander }}} set out with <a href="/army/{{{ $challenge->challenger->id }}}">{{{ $challenge->challenger->name }}}</a>
      @if ($challenge->generations < 75)
          to fight a short skirmish.
      @elseif ($challenge->generations < 175)
          to resolve a territorial dispute.
      @elseif ($challenge->generations < 275)
          to find a battle.
      @elseif ($challenge->generations < 375)
          to lay a siege.
      @else
          to wage a war.
      @endif
    @if ($myarmy)
      @if ($challenge->challenger->id == $myarmy->id || (Auth::check() && ($challenge->challenger->owner && ($challenge->challenger->owner == Auth::user()->id))))
        <p>Waiting for a challenger to accept</p>
        <p>
          Ask your friends to fight your army:
        </p>
        <p>
          <div class="fb-send" data-href="https://developers.facebook.com/docs/plugins/" data-colorscheme="light"></div>
          <a class="twitter-share-button"
             href="https://twitter.com/share"
             data-count="none"
             data-text="Fight my army on Game Of Life: Total War!"
             data-hashtags="gameoflife">
           Tweet
           </a>
           <script type="text/javascript">
           window.twttr=(function(d,s,id){var t,js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return}js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);return window.twttr||(t={_e:[],ready:function(f){t._e.push(f)}})}(document,"script","twitter-wjs"));
           </script>
           <div class="g-plus" data-action="share" data-annotation="none"></div>
        </p>
      @else
        @if ($myarmy->ready())
	      @if (Session::get('rate_limit', null) !== null)
	      <div id="captchacopy">
	      </div>
	      @endif
          <p>
            <input type="button" class="btn btn-success" id="challenge_accept" value="Accept The Challenge!" />
          </p>
        @else
          <p>Your army is already embroiled in conflict. Pick another army to accept this challenge</p>
        @endif
      @endif
    @else
      <p><a href="javascript:$('#create').modal('toggle');void(0);">Create an army</a> to accept the challenge!</p>
    @endif
  @endif
  @if ($challenge->ready !== null)
    <div id="statistics">
      <div id="battlebar">
        <div id="bb-midpoint"></div>
        <div id="bb-at" class="battlebar-segment"></div>
        <div id="bb-ci" class="battlebar-segment"></div>
        <div id="bb-de" class="battlebar-segment"></div>
      </div>
      <div id="battlechart"></div>
    </div>
  @endif
</div>
@stop

@section('pagejs')
@if ($challenge->ready !== null)
{{ HTML::script('/js/canvasjs.min.js') }}
@endif
@if (Session::get('rate_limit', null) !== null)
<script defer="defer">
$(document).ready(function () {
    setTimeout(function () {
        $('#challenge').hide(function () {
            if ($('#captchamain').html().length) {
                $('#captchacopy').html($('#captchamain').clone(true, true));
                $('#captchamain').children('.captcha').remove();
            }
        });
        $('#challengepopup').bind("mouseup", function () {
            $('#captchamain').html($('#captchacopy').clone(true, true));
            $('#captchacopy').children('.captcha').remove();
        });
    }, 500);
});
</script>
@endif
@stop