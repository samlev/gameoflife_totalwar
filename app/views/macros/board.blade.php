@section('macrostyle')
<style>
.displayboard td {
    width: {{ceil(600/$challenge->width)}}px;
    height: {{ceil(600/$challenge->width)}}px;
}
</style>
@stop
<div id="board">
  <canvas id="boardcanvas"></canvas>
<div>
  <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">Attackers: <span class="boarddata" id="challengers">0</span></div>
  <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">Civilians: <span class="boarddata" id="civilians">0</span></div>
  <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">Defenders: <span class="boarddata" id="defenders">0</span></div>
  <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">Generations: <span class="boarddata" id="generation">0</span></div>
</div>
</div>
@section('macrojs')
<script>
var board = {{$challenge->board_start}};
var width = {{$challenge->width}};
var height = {{$challenge->height}};
var generations = {{$challenge->generations}};
</script>
{{ HTML::script('/js/gol.canvas.js') }}
{{ HTML::script('/js/boardcommon.js') }}
{{ HTML::script('/js/board.js') }}
@stop