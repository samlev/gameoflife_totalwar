@section('macrostyle')
<style>
.displayboard td {
    width: {{ceil(600/$challenge->width)}}px;
    height: {{ceil(600/$challenge->width)}}px;
}
</style>
@stop
<div id="board">
    <canvas id="boardcanvas"></canvas>
<div>
  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">Army Strength: <span class="boarddata" id="strength">0</span></div>
  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">Civilians: <span class="boarddata" id="civilians">0</span></div>
</div>
<div>
  Waiting for the @if ($myarmy->id == $challenge->challenger->id) defender @else challenger @endif to finish placing their army.
</div>
</div>
@section('macrojs')
<script>
var board = {{$challenge->board_start}};
var width = {{$challenge->width}};
var height = {{$challenge->height}};
@if ($myarmy->id == $challenge->challenger->id)
var my_board = {{$challenge->challenger_start}};
var start_x = 0;
var end_x = {{ $challenge->command_width }};
var live = 2;
@else
var my_board = {{$challenge->defender_start}};
var start_x = {{ $challenge->width - $challenge->command_width}};
var end_x = {{$challenge->width}};
var live = 3;
@endif
var battle = "{{{$challenge->battle}}}";
</script>
{{ HTML::script('/js/gol.canvas.js') }}
{{ HTML::script('/js/boardcommon.js') }}
{{ HTML::script('/js/boardwait.js') }}
@stop