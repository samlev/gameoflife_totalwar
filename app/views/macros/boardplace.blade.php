@section('macrostyle')
<style>
.displayboard td {
    width: {{ceil(600/$challenge->width)}}px;
    height: {{ceil(600/$challenge->width)}}px;
}
</style>
@stop
<div>
    <canvas id="boardcanvas"></canvas>
    <div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">Army Left to Place: <span class="boarddata" id="strength">{{$myarmy->strength}}</span></div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">Civilians: <span class="boarddata" id="civilians">0</span></div>
    </div>
    <div>
      <p>Is everything just how you want it? Your starting formation is final!
      <input type="button" class="btn btn-success" id="board_ready" value="I'm ready for battle" /></p>
    </div>
</div>
@section('macrojs')
<script>
var board = {{$challenge->board_start}};
var width = {{$challenge->width}};
var height = {{$challenge->height}};
var generations = {{$challenge->generations}};
@if ($myarmy->id == $challenge->challenger->id)
var start_x = 0;
var end_x = {{ $challenge->command_width }};
var live = 2;
@else
var start_x = {{ $challenge->width - $challenge->command_width}};
var end_x = {{$challenge->width}};
var live = 3;
@endif
var strength = {{$myarmy->strength}};
var battle = "{{{$challenge->battle}}}";
</script>
{{ HTML::script('/js/gol.canvas.js') }}
{{ HTML::script('/js/boardcommon.js') }}
{{ HTML::script('/js/boardplace.js') }}
@stop