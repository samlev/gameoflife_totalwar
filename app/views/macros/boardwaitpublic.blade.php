@section('macrostyle')
<style>
.displayboard td {
    width: {{ceil(600/$challenge->width)}}px;
    height: {{ceil(600/$challenge->width)}}px;
}
</style>
@stop
<div id="board">
    <canvas id="boardcanvas"></canvas>
<div>
  <div>Civilians: <span class="boarddata" id="civilians">0</span></div>
</div>
<div>
  @if ($challenge->defender === null)
    Waiting for a challenger
  @else
    Waiting for both teams to place their pieces
  @endif
</div>
</div>
@section('macrojs')
<script>
var board = {{$challenge->board_start}};
var width = {{$challenge->width}};
var height = {{$challenge->height}};
var battle = "{{{$challenge->battle}}}";
</script>
{{ HTML::script('/js/gol.canvas.js') }}
{{ HTML::script('/js/boardcommon.js') }}
{{ HTML::script('/js/boardwaitpublic.js') }}
@stop