@extends('__master')

@section('pagetitle')
Technology behind Game of Life - Total War!
@stop

@section('pagecontent')
<div class="row">
<div class="lead">
<h2 class="section-heading">
Technology behind Game of Life: Total War
</h2>
  <p>
    <strong>Game of Life: Total War</strong> was made with a mixture of technologies. These include:
  </p>
  <ul>
    <li><a href="http://laravel.com/" target="_blank">Laravel PHP Framework</a> - used for most of the visible site.</li>
    <li><a href="http://getbootstrap.com/" target="_blank">Bootstrap</a> - used for basic skinning and components.</li>
    <li><a href="http://canvasjs.com/" target="_blank">CanvasJS Charts</a> - used for challenge statistics.</li>
    <li>
      <a href="http://startbootstrap.com/template-overviews/landing-page/" target="_blank">Landing Page</a> - a
      bootstrap theme which I <em>heavily</em> bastardised.
    </li>
    <li>
      <a href="http://pmav.eu/stuff/javascript-game-of-life-v3.1.1/" target="_blank">Javascript Game Of Life</a> -
      heavily modified, and taken just for the really nice canvas implementation of drawing and user interaction.
    </li>
    <li>
      <a href="http://www.quasimondo.com/BoxBlurForCanvas/FastBlurDemo.html" target="_blank">Box Blur for Canvas</a> -
      heavily modified, and taken to provide a simple Fog Of War filter.
    </li>
    <li>
      <a href="http://www.pylonsproject.org/" target="_blank">Pyramid</a> - used to process games in the background to
      calculate winners and losers in each battle.
    </li>
    <li>
      <a href="http://www.rabbitmq.com/" target="_blank">RabbitMQ</a> - used to provide off-load queueing of games to
      process.
    </li>
    <li>
      <a href="https://github.com/gjedeer/celery-php" target="_blank">PHP Celery</a> - used to send games to RabbitMQ for
      processing.
    </li>
    <li>
      <a href="https://github.com/sontek/pyramid_celery" target="_blank">Pyramid Celery</a> - used to fetch games for
      processing from RabbitMQ.
    </li>
    <li>
      <a href="http://supervisord.org/" target="_blank">SupervisorD</a> - used to run pyramid celery workers.
    </li>
    <li>
      <a href="https://www.linode.com/" target="_blank">Linode</a> - used for hosting this monstrosity.
    </li>
  </ul>
</div>
</div>
@stop