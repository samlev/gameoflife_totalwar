@extends('__master')

@section('pagetitle')
How to play Game of Life - Total War!
@stop

@section('pagecontent')
<div class="row">
<div class="lead">
<h2 class="section-heading">
How to play Game of Life: Total War
</h2>
  <p>
    <strong>Game of Life: Total War</strong> is a multiplayer variant of Conways Game of Life.
  </p>
  <p>
    In Conway's Game of Life, cells live and die based on simple rules.
  </p>
  <ul>
    <li>Living cells with less than two neighbours die, as if by loneliness.</li>
    <li>Living cells with more than three neighbours die, as if by overcrowding.</li>
    <li>Dead cells with exactly three living neighbours spring to life.</li>
  </ul>
  <p>
    In the Total War version of Game of Life, I have also added armies. Each battle consists of one army attacking or
    challenging another. Added to the mix are the neutral 'civilians'. In this version, a new cell is spawned with
    respect to it's parentage. If the majority of the parents (2 or 3) are attackers, then the new cell will also be an
    attacker; the same logic also applies for defenders. If there's no clear parentage, then new cells become
    conflicted, and are civilians.
  </p>
  <img src="/img/create-an-army.png" style="margin-left: 5px;" class="img-responsive pull-right" alt="Create An Army">
<h3 class="section-heading">
Creating an Army
</h3>
  <p>
    The first thing you'll need is an army.
  </p>
  <p>
    All armies start with 100 troops. Each army must have a unique name, and a leader.
  </p>
  <p>
    After each battle, your army will either gain troops for a win, or lose troops from a loss. The number of troops in
    your army affects how many troops you can start a battle with. More troops means bigger, more complex formations.
  </p>
  <p>
    If you want to save your army, you must log in or create an account. Creating an account also allows you to create
    and keep up to five armies.
  </p>
  <img src="/img/challenge-someone.png" style="margin-right: 5px;" class="img-responsive pull-left" alt="Challenge Someone">
<h3 class="section-heading">
Starting a Battle
</h3>
  <p>
    When you have an army, you can start your own challenge (making you the attacker).
  </p>
  <p>
    To start a battle, you have to select the length of the battle, and the size of the field.
  </p>
  <p>
    When you start your battle, a number of civilians will be placed randomly on the board, concentrated mostly in the
    centre of the board.
  </p>
  <p>
    Larger sizes may be more difficult to place your troops, but they have more room for larger, more complex
    formations.
  </p>
  <p>
    If your army wins enough battles, and grows large enough, it will unlock even larger battle fields to play on.
  </p>
  <img src="/img/place-army.png" style="margin-left: 5px;" class="img-responsive pull-right" alt="Place Your Army">
<h3 class="section-heading">
Placing your army
</h3>
  <p>
    You can place your troops on one section of the board (attackers can use the left 40% of the board, defenders can
    use the right 45% of the board). The rest of the board is under 'fog of war' until both sides have place their army.
  </p>
  <p>
    Placing your troops is simple - simply click on a square where you want a soldier to be. You can also remove troops
    by clicking on them. You cannot remove civilians, though, so you will have to design your formations around them.
  </p>
  <p>
    The goal is to have the most troops left at the end of the battle, so try building formations that grow, or that
    will disrupt your opponent's structures. Remember also that civilians may get unruly and ruin your plans.
  </p>
  <p>
    Once you're entirely satisfied with your army's placement, click "I'm ready for battle". Once you submit your
    formation, you won't be able to change it.
  </p>
  <p>
    At any point after you create the challenge, you can give the link to a friend, either directly, or using one of the
    'share' buttons. The first person to accept the challenge takes it, though, so if you <em>really</em> want to
    challenge a specific friend, send them the link directly.
  </p>
  <p>
    When both teams have placed their armies, the battle will commence automatically.
  </p>
<h3 class="section-heading">
Winning a battle
</h3>
  <p>
    The objective of a battle is to get to the final generation with as many surviving troops as possible. Either team
    running out of living troops entirely will cause the battle to end then, and the remaining generations won't be
    played.
  </p>
  <p>
    The reward or punishment for winning or losing depends on if you are attacking or defending. Winning a battle while
    attacking will grow your army's strength by 10, but losing a battle while attacking will lose you 10 strength. The
    risks for defending are lower, but so are the rewards. A defensive win will only gain you 5 troops, but a loss
    will only lose you 5.
  </p>
  <p>
    All ties are decided in favour of the defender.
  </p>
</div>
</div>
@stop