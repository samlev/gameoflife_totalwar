@extends('__master')

@section('pagetitle')
About Game of Life - Total War!
@stop

@section('pagecontent')
<div class="row">
<div class="lead">
<h2 class="section-heading">
About Game of Life: Total War
</h2>
  <p>
    This was made as an experiment, and as an excuse to test out different combinations of
    <a href="/technology">technologies</a>. The idea of a multi-player "Game of Life" variant has been something that I
    have toyed with before, but this is the version that I always thought about creating.
  </p>
  <p>
    The rules may change with time, but the basic idea is the important part to me: "What would happen if two people
    played Game of Life <em>against</em> each other?"
  </p>
  <p>
    Well now we may find an answer. I'm interested to see what types of strategy arise (personally, I am a fan of trying
    to build a glider gun). Whichever way it goes, it was a fun experiment, and I'll keep improving it over time.
  </p>
  <p>
    If you're really interested in how it's built, check out the <a href="/technology">technology</a> page, or feel free
    to <a href="http://www.samuellevy.com/contact" target="blank">contact me</a>.
  </p>
</div>
</div>
@stop