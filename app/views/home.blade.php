@extends('__master')

@section('pagetitle')
Game of Life: Total War
@stop

@section('pagecontent')
<div class="row">
@if ($challenge = Challenge::whereNotNull('ready')->orderBy('updated_at', 'DESC')->first())
  <div class="pull-right">
  <h2>The latest <a href="/challenge/{{{$challenge->battle}}}">battle to be fought</a>!</h2>
  @include('macros.board')
  </div>
@endif

<div class="lead">
  <h2 class="section-heading">
    Game Of Life: Total War
  </h2>
  <p>
    <strong>Game of Life: Total War</strong> is a multiplayer variant of Conways Game of Life.
  </p>
  <p>
    Unlike other variations of Game Of Life, you place pieces in an attempt to out-thrive or out-survive your opponent.
    Each battle is between an attacker, a defender, and a whole bunch of unruly civilians.
  </p>
  <p>
    Winning a battle lets you grow your army to build bigger and better formations, but losing a battle will cost you
    valuable recruits.
  </p>
  <p>
    You can create an army and get right into the fight, or you can create an account to create and save multiple
    armies, which you can use to battle your friends.
  </p>
</div>
</div>
<div class="row clearfix">
@if (Challenge::whereNotNull('ready')->count() > 0)
<div class="lead col-xs-12 col-sm-12 col-md-6 col-lg-6 pull-left">
  <h3 class="section-heading">Recent Battles</h3>
  @foreach (Challenge::whereNotNull('ready')->orderBy('updated_at', 'DESC')->take(10)->get() as $c)
    <p>
      <a href="/army/{{{ $c->challenger->id }}}">{{{ $c->challenger->name }}}</a>
      @if ($c->generations < 75)
          had a <a href="/challenge/{{{$c->battle}}}">short skirmish</a> with
      @elseif ($c->generations < 175)
          had a <a href="/challenge/{{{$c->battle}}}">territorial dispute</a> with
      @elseif ($c->generations < 275)
          had a <a href="/challenge/{{{$c->battle}}}">battle</a> with
      @elseif ($c->generations < 375)
          laid a <a href="/challenge/{{{$c->battle}}}">siege</a> against
      @else
          waged <a href="/challenge/{{{$c->battle}}}">war</a> against
      @endif
      <a href="/army/{{{ $c->defender->id }}}">{{{ $c->defender->name }}}</a>
    </p>
  @endforeach
</div>
@endif
@if (Challenge::whereNull('defender_id')->whereNotNull('challenger_complete')->count() > 0)
<div class="lead col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-left">
  <h3 class="section-heading">Unmet Challenges</h3>
  @foreach (Challenge::whereNull('defender_id')->whereNotNull('challenger_complete')->orderBy('created_at', 'ASC')->take(5)->get() as $c)
    <p>
      {{{ $c->timeAgo() }}} <a href="/army/{{{ $c->challenger->id }}}">{{{ $c->challenger->name }}}</a> set out
      @if ($c->generations < 75)
          to fight a short skirmish.
      @elseif ($c->generations < 175)
          to resolve a territorial dispute.
      @elseif ($c->generations < 275)
          to find a battle.
      @elseif ($c->generations < 375)
          to lay a siege.
      @else
          to wage a war.
      @endif
      <a href="/challenge/{{{$c->battle}}}">View the challenge</a>.
    </p>
  @endforeach
</div>
@endif
@if (Army::where('active', '=', 1)->count() > 0)
<div class="lead col-xs-12 col-sm-12 col-md-5 col-lg-5 pull-left">
  <h3 class="section-heading">Strongest Armies</h3>
  @foreach (Army::where('active', '=', 1)->orderBy('strength', 'DESC')->orderBy('victories', 'DESC')->orderBy('losses', 'DESC')->take(5)->get() as $a)
    <p>
      <a href="/army/{{{ $a->id }}}">{{{ $a->name }}}</a> led by {{{ $a->commander }}}.
      <br>
      {{{ ($a->victories == 1 ? '1 victory' : $a->victories.' victories') }}} and
      {{{ ($a->losses == 1 ? '1 defeat' : $a->losses.' defeats') }}},
      currently {{{ $a->strength }}} soldiers strong.
    </p>
  @endforeach
</div>
@endif
</div>
@stop