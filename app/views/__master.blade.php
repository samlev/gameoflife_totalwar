<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>@yield('pagetitle')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css') }}
  {{ HTML::style('/font-awesome/css/font-awesome.min.css') }}
  {{ HTML::style('//fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic') }}
  {{ HTML::style('/css/custom.css') }}
  {{ HTML::style('/css/style.css') }}
  <style type="text/css">
body {
    padding-top: 60px;
    padding-bottom: 40px;
}
</style>
  @yield('pagestyle')
  @yield('macrostyle')
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<input type="hidden" id="_token" value="{{ csrf_token() }}">
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/">Game of Life: Total War</a>
  </div><!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/how-to-play" data-id="how-to-play">How To Play</a></li>
      @if (Session::get('army_id', null) === null)
        @if (Auth::check())
          @if (Auth::user()->armies()->where('active', '=', '1')->count() > 0)
          <li class="dropdown" id="armymenu">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Armies<b class="caret"></b></a>
            <ul class="dropdown-menu">
            @if (Auth::user()->armies()->where('active', '=', '1')->count() < 5)
              <li>
                <a href="#" class="scroll-link" id="createpopup">Create an Army
                ({{{ 5 - Auth::user()->armies()->where('active', '=', '1')->count() }}} left)</a>
              </li>
                @if (Auth::user()->armies()->where('active', '=', '1')->count() > 0)
                <li class="divider"></li>
                @endif
            @endif
            @if (Auth::user()->armies()->where('active', '=', '1')->count() > 0)
              @foreach (Auth::user()->armies()->where('active', '=', 1)->get() as $a)
                <li>
                  <a href="javascript: selectArmy({{{$a->id}}}); void(0);">Fight with <strong>{{{$a->name}}}</strong>
                  @if ($a->ready()) (Strength: {{{$a->strength}}}) @else() (in battle) @endif</a>
                </li>
              @endforeach
            @endif
            </ul>
            </li>
          @else
            <li><a href="#" class="scroll-link" id="createpopup">Create an Army</a></li>
          @endif
        @else
          <li><a href="#" class="scroll-link" id="createpopup">Create an Army</a></li>
        @endif
      @else
        <?php $myarmy = isset($myarmy) ? $myarmy : Army::find(Session::get('army_id')); ?>
        @if ($myarmy)
          <li class="dropdown" id="armymenu">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Fighting with: {{{$myarmy->name}}}<b class="caret"></b></a>
            <ul class="dropdown-menu">
          <li><a href="/army/{{{$myarmy->id}}}">{{{$myarmy->name}}} ({{{$myarmy->strength}}})</a></li>
          @if ($myarmy->ready())
          <li><a href="#" class="scroll-link" id="editpopup">Edit this Army</a></li>
          <li><a href="#" class="scroll-link" id="challengepopup">Challenge Someone</a></li>
          @elseif ($myarmy->active == 1)
          <li><a href="/challenge/{{{$myarmy->current_challenge()->battle}}}">Current Challenge</a></li>
          @endif
          @if (Auth::check())
            @if (Auth::user()->armies()->where('active', '=', '1')->count() < 5)
              <li class="divider"></li>
              <li><a href="#" class="scroll-link" id="createpopup">Create an Army
              ({{{ 5 - Auth::user()->armies()->where('active', '=', '1')->count() }}} left)</a></li>
            @endif
            @if (Auth::user()->armies()->where('active', '=', '1')->count() > 1)
              <li class="divider"></li>
              @foreach (Auth::user()->armies()->where('active', '=', 1)->get() as $a)
                @if ($myarmy->id != $a->id)
                <li>
                  <a href="javascript: selectArmy({{{$a->id}}}); void(0);">Fight with <strong>{{{$a->name}}}</strong>
                  @if ($a->ready()) (Strength: {{{$a->strength}}}) @else() (in battle) @endif</a>
                </li>
                @endif
              @endforeach
            @endif
          @endif
            </ul>
          </li>
        @endif
      @endif
      @if (Auth::check())
        <li class="dropdown" id="usermenu">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{{Auth::user()->display}}}<b class="caret"></b></a>
            <ul class="dropdown-menu">
            <li><a href="javascript:doLogout();void(0);" style="cursor:pointer;">Log out</a></li>
        </ul>
        </li>
      @else
        <li><a style="cursor:pointer;" id="loginpopup">Login / Signup</a></li>
      @endif
    </ul>
  </div><!-- /.navbar-collapse -->
</nav>
<div id="ad1" class="hidden-xs hidden-sm">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- gameoflife left -->
<ins class="adsbygoogle"
     style="display:inline-block;width:160px;height:600px"
     data-ad-client="ca-pub-8623364004727745"
     data-ad-slot="5973947318"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div id="ad2" class="hidden-xs hidden-sm">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- gameoflife right-proper -->
<ins class="adsbygoogle"
     style="display:inline-block;width:160px;height:600px"
     data-ad-client="ca-pub-8623364004727745"
     data-ad-slot="4357613318"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
@if (Auth::check() === false)
<!-- Code for Login / Signup Popup -->
  <!-- Modal Log in -->
	<form class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	  <div class="modal-dialog" style="margin-top: 150px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel1">Login</h4>
	      </div>
	      <div class="modal-body" id="login_details">
	        <div id="login_errors"></div>
	        <span> Already have an account?</span><br><br>
	        <span style="font-weight:bold;">Login Name</span><br>
	        <input type="text" placeholder="Username" id="login_name" /><br><br>
	        <span style="font-weight:bold;" >Password</span><br>
	        <input type="password" placeholder="Password" id="login_password" /><br><br>
			<span> Forgot your password?</span>
			<span style="cursor:pointer;" class="text-info forgot-link">Reset it!</span>
	      </div>
	      <div class="modal-footer">
			<input style="float: left" type="submit" class="btn btn-success" value="Log In" />
			<span> Not a member yet?</span>
			<span style="cursor:pointer;" class="text-info signup-link">Sign Up!</span>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</form><!-- /.modal -->
 <!--Modal Login Ends -->
 <!-- Modal Sign-up Starts -->
	<form class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
	  <div class="modal-dialog" style="margin-top: 100px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel2">Sign Up</h4>
	      </div>
	      <div class="modal-body" id="signup_details">
	        <div id="signup_errors"></div>
	        *<span>Login name</span>
	        <input type="text" placeholder="Username" id="register_name" /><br><br>
	        <span>Email</span>
	        <input type="text" placeholder="example@gmail.com" id="register_email" /><br><br>
	        *<span>Password</span>
	        <input type="password" placeholder="Password" id="register_password" /><br><br>
	        *<span>Confirm Password</span>
	        <input type="password" placeholder="Re-type Password" id="re_register_password" />
	      </div>
	      <div class="modal-footer" >
		<input style="float: left" type="submit" class="btn btn-success"  value="Sign Me Up" />
	       <span>&nbsp;&nbsp;&nbsp; Already a member? </span><span class="text-info login-link" style="cursor:pointer;">  Login now  </span>
		 </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</form><!-- /.modal -->
  <!-- Modal Sign up ends! -->
  <!-- Modal Forgot password -->
	<form class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel7" aria-hidden="true">
	  <div class="modal-dialog" style="margin-top: 150px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel1">Forgot Password</h4>
	      </div>
	      <div class="modal-body" id="login_details">
	        <div id="forgot_errors"></div>
	        <span> Already have an account?</span><br><br>
	        <span style="font-weight:bold;">User Name</span><br>
	        <input type="text" placeholder="User Name" id="forgot_username" /><br><br>
			<span> Remembered your password?</span>
			<span style="cursor:pointer;" class="text-info login-link">Login now</span>
	      </div>
	      <div class="modal-footer">
			<input style="float: left" type="submit" class="btn btn-success" id="forgot_submit" value="Reset my password!" />
			<span> Not a member yet?</span>
			<span style="cursor:pointer;" class="text-info signup-link">Sign Up!</span>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</form><!-- /.modal -->
 <!--Modal Forgot password Ends -->
@endif

 <!-- Modal Create Army Starts -->
	<form class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	  <div class="modal-dialog" style="margin-top: 150px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel2">Create Army</h4>
	      </div>
	      <div class="modal-body" id="army_details">
	        <div id="create_errors"></div>
	        <span>Army name</span><br>
	        <input type="text" placeholder="Army name" id="army_name" /> <br><br>
	        <span>Led by</span><br>
	        <input type="text" placeholder="Commander's Name" id="army_commander" />
	      </div>
	      <div class="modal-footer" >
		<input style="float: left" type="submit" class="btn btn-success"  value="Raise my Army" />
		 </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</form><!-- /.modal -->
  <!-- Modal Create Army ends! -->

@if (isset($myarmy))
 <!-- Modal Edit Army Starts -->
	<form class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel6" aria-hidden="true">
	  <div class="modal-dialog" style="margin-top: 150px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel2">Create Army</h4>
	      </div>
	      <div class="modal-body" id="edit_army_details">
	        <div id="edit_errors"></div>
	        <input type="hidden" id="army_id" value="{{{$myarmy->id}}}">
	        <input type="hidden" id="commander_current" value="{{{$myarmy->commander}}}">
	        <span>Army name</span><br>
	        {{{$myarmy->name}}} <br><br>
	        <span>Led by</span><br>
	        <input type="text" placeholder="Commander's Name" id="edit_army_commander" value="{{{$myarmy->commander}}}" />
	      </div>
	      <div class="modal-footer" >
		  @if(Auth::check())
           <input style="float: right" type="button" class="btn btn-danger" id="disband" value="Disband Army" />
		  @endif
		  <input style="float: left" type="submit" class="btn btn-success" value="Save my Army" />
		 </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</form><!-- /.modal -->
  <!-- Modal Edit Army ends! -->
 <!-- Modal Create Challenge Starts -->
	<form class="modal fade" id="challenge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" aria-hidden="true">
	  <div class="modal-dialog" style="margin-top: 150px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel2">Challenge Someone</h4>
	      </div>
	      <div class="modal-body" id="challenge_details">
	        <span>Battle Length</span><br>
	        <select id="challenge_length">
	          <option value="50">Short Skirmish</option>
	          <option value="100">Territorial Dispute</option>
	          <option selected value="200">Regular Battle</option>
	          <option value="300">Siege</option>
	          <option value="400">Total War</option>
	        </select> <br><br>
	        <span>Battlefield Size</span><br>
	        <select id="challenge_size">
	          <option value="80">Building</option>
	          <option value="100">Urban Warfare</option>
	          <option selected value="120">City Block</option>
	          @if ($myarmy->strength >= 150)
	          <option value="150">Open Field</option>
	          @endif
	          @if ($myarmy->strength >= 300)
	          <option value="200">Long Range Warfare</option>
	          @endif
	        </select> <br><br>
	        @if (Session::get('rate_limit', null) !== null)
	          <div id="captchamain">
	            <div class="captcha">
	            <h4>
	              Woah there {{{ (Auth::check() ? Auth::user()->display : $myarmy->commander) }}}!
	            </h4>
	            <p>
	              You've been entering
	              so many challenges that we think you might be a bot. Bots make the game less fun for everyone else, so
	              you'll be getting captchaed for a while.
	            </p>
	          {{ Form::captcha() }}
	            </div>
	          </div>
	        @endif
	      </div>
	      <div class="modal-footer" >
            <input style="float: left" type="submit" class="btn btn-success"  value="Create the Challenge" />
		  </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</form><!-- /.modal -->
  <!-- Modal Create Challenge ends! -->
@endif
  <!-- End code for Login / Signup Popup -->
<div class="container">
 <div class="clearfix">
 @yield('pagecontent')
 </div>
 <footer>
   <p class="pull-right">&copy; <a href="http://www.samuellevy.com/">Samuel Levy</a> {{ date('Y') }}</p>
   <p>
     <a href="/about">About</a> |
     <a href="/technology">Technology</a> |
     <a href="/privacy">Privacy</a>
   </p>
 </footer>
</div>
  {{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js') }}
  {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js') }}
  {{ HTML::script('/js/utils.js') }}
  {{ HTML::script('/js/custom.js') }}
  {{ HTML::script('/js/main.js') }}
  @yield('pagejs')
  @yield('macrojs')
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4526160-8', 'auto');
  ga('send', 'pageview');

</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
</body>
</html>