/*jslint onevar: true, undef: false, nomen: true, eqeqeq: true, plusplus: false, bitwise: true, regexp: true, newcap: true, immed: true  */

/**
 * Game of Life - JS & CSS
 * http://pmav.eu
 * 04/Sep/2010
 */

var GOL = {
    columns : 0,
    rows : 0,

    /**
     * On Load Event
     */
    init : function() {
        try {
            this.canvas.init();     // Init canvas GUI
            this.prepare();
        } catch (e) {
            alert("Error: "+e);
        }
    },


    /**
     * Prepare DOM elements and Canvas for a new run
     */
    prepare : function() {
        this.canvas.drawWorld(); // Draw State
    },

    /** ****************************************************************************************************************************
     *
     */
    canvas: {
        canvas: null,
        context : null,
        width : null,
        height : null,
        age : null,
        cellSize : 0,
        cellSpace : 1,


        /**
         * init
         */
        init : function() {
            this.canvas = document.getElementById('boardcanvas');
            this.context = this.canvas.getContext('2d');

            this.cellSpace = 1;
            this.cellSize = Math.floor(600 / width) - this.cellSpace;
        },

        /**
         * drawWorld
         */
        drawWorld : function() {
            this.width = this.height = 1;

            // Dynamic canvas size
            this.width = this.width + (this.cellSpace * width) + (this.cellSize * width);
            this.canvas.setAttribute('width', this.width);

            this.height = this.height + (this.cellSpace * height) + (this.cellSize * height);
            this.canvas.setAttribute('height', this.height);

            // Fill background
            this.context.fillStyle = '#EEEEEE';
            this.context.fillRect(0, 0, this.width, this.height);

            for (var y = 0 ; y < board.length; y ++) {
                var row = board[y];
                for (var x = 0 ; x < row.length; x ++) {
                    this.drawCell(x, y, row[x]);
                }
            }
        },

        /**
         * drawCell
         */
        drawCell : function (i, j) {
            switch (board[j][i]) {
                case 1:
                    // civilian
                    this.context.fillStyle = '#999922';
                    break;
                case 2:
                    // attacker/challenger
                    this.context.fillStyle = '#992222';
                    break;
                case 3:
                    // defender
                    this.context.fillStyle = '#222299';
                    break;
                default:
                    // dead
                    this.context.fillStyle = '#FFFFFF';
                    break;
            }

            this.context.fillRect(this.cellSpace + (this.cellSpace * i) + (this.cellSize * i), this.cellSpace + (this.cellSpace * j) + (this.cellSize * j), this.cellSize, this.cellSize);

        }
    },
    helpers : {
        /**
         * Register Event
         */
        registerEvent : function (element, event, handler, capture) {
            if (/msie/i.test(navigator.userAgent)) {
                element.attachEvent('on' + event, handler);
            } else {
                element.addEventListener(event, handler, capture);
            }
        },


        /**
         *
         */
        mousePosition : function (e) {
            // http://www.malleus.de/FAQ/getImgMousePos.html
            // http://www.quirksmode.org/js/events_properties.html#position
            var event, x, y, domObject, posx = 0, posy = 0, top = 0, left = 0, cellSize = GOL.canvas.cellSize + 1;

            event = e;
            if (!event) {
                event = window.event;
            }

            if (event.pageX || event.pageY) 	{
                posx = event.pageX;
                posy = event.pageY;
            } else if (event.clientX || event.clientY) 	{
                posx = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }

            domObject = event.target || event.srcElement;

            while ( domObject.offsetParent ) {
                left += domObject.offsetLeft;
                top += domObject.offsetTop;
                domObject = domObject.offsetParent;
            }

            domObject.pageTop = top;
            domObject.pageLeft = left;

            x = Math.ceil(((posx - domObject.pageLeft)/cellSize) - 1);
            y = Math.ceil(((posy - domObject.pageTop)/cellSize) - 1);

            return [x, y];
        }
    }
};