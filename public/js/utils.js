/**
 * Created by sam on 15/10/14.
 */
function clamp(num, min, max) {
    return Math.max(Math.min(num, max), min);
}