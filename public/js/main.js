$(document).ready(function () {
    $('#login').submit(doLogin);
    $('#forgot').submit(doForgot);
    $('#signup').submit(doRegister);
    $('#create').submit(doCreate);
    $('#challenge').submit(doChallenge);
    $('#edit').submit(doEdit);
    $('#disband').click(doDisband);
    $('#reset').submit(doReset);
});

function doLogin(event) {
    event.preventDefault();

    var username = $('#login_name').val();
    var password = $('#login_password').val();

    var posting = $.post('/api/v1/login', {
        username: username,
        password: password,
        _token: $('#_token').val()
    }, 'json');

    posting.done(function (data) {
        window.location.reload();
    });

    posting.fail(function (data) {
        $('#login_errors').html('<div class="alert alert-warning alert-dismissible" role="alert">'+
            '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+
            data.responseJSON.error + '</div>');
    });
}

function doForgot(event) {
    event.preventDefault();

    var username = $('#forgot_username').val();
    $('#forgot_submit').attr('disabled', 'disabled');

    var posting = $.post('/api/v1/forgot-password', {
        username: username,
        _token: $('#_token').val()
    }, 'json');

    posting.done(function (data) {
        if (data.status !== undefined) {
            $('#forgot_errors').html('<div class="alert alert-success alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                data.status + '</div>');
        } else if (data.error !== undefined) {
            $('#forgot_errors').html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+
                data.error + '</div>');
        }
        $('#forgot_submit').attr('disabled', null);
    });

    posting.fail(function (data) {
        if (data.responseJSON.error !== undefined) {
            $('#forgot_errors').html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+
                data.responseJSON.error + '</div>');
        }
        $('#forgot_submit').attr('disabled', null);
    });
}

function doReset(event) {
    event.preventDefault();

    var token = $('#reset_token').val();
    var username = $('#reset_username').val();
    var password = $('#reset_password').val();
    var password_confirmation = $('#reset_password_confirmation').val();
    $('#reset_submit').attr('disabled', 'disabled');

    var posting = $.post('/api/v1/reset-password', {
        username: username,
        password: password,
        password_confirmation: password_confirmation,
        token: token,
        _token: $('#_token').val()
    }, 'json');

    posting.done(function (data) {
        if (data.error !== undefined) {
            $('#reset_errors').html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+
                data.error + '</div>');
            $('#reset_submit').attr('disabled', null);
        } else {
            window.location = '/';
        }
    });

    posting.fail(function (data) {
    });
}

function doRegister(event) {
    event.preventDefault();

    var username = $('#register_name').val();
    var email = $('#register_email').val();
    var password = $('#register_password').val();
    var confirm = $('#re_register_password').val();

    var posting = $.post('/api/v1/register', {
        username: username,
        email: email,
        password: password,
        confirm: confirm,
        _token: $('#_token').val()
    }, 'json');

    posting.done(function (data) {
        window.location.reload();
    });

    posting.fail(function (data) {
        var errors = '';
        for(var index in data.responseJSON.errors) {
            if (data.responseJSON.errors.hasOwnProperty(index)) {
                errors += '<div class="alert alert-warning alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                    data.responseJSON.errors[index][0] + '</div>';
            }
        }

        $('#signup_errors').html(errors);
    });
}

function doLogout() {
    var posting = $.post('/api/v1/logout', {_token: $('#_token').val()}, 'json');

    posting.done(function (data) {
        window.location.reload();
    });

    posting.fail(function (data) {
        alert(data.responseJSON.error)
    });
}

function doCreate(event) {
    event.preventDefault();

    var name = $('#army_name').val();
    var commander = $('#army_commander').val();

    var posting = $.post('/api/v1/army', {
        name: name,
        commander: commander,
        _token: $('#_token').val()
    }, 'json');

    posting.done(function (data) {
        window.location.reload();
    });

    posting.fail(function (data) {
        var errors = '';
        for(var index in data.responseJSON.errors) {
            if (data.responseJSON.errors.hasOwnProperty(index)) {
                errors += '<div class="alert alert-warning alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                data.responseJSON.errors[index][0] + '</div>';
            }
        }

        $('#create_errors').html(errors);
    });
}

function selectArmy(id) {
    var posting = $.post('/api/v1/army/'+id+'/select', {
        _token: $('#_token').val()
    }, 'json');

    posting.done(function (data) {
        window.location.reload();
    });

    posting.fail(function (data) {
        alert(data.responseJSON.error);
    });
}

function doChallenge(event) {
    event.preventDefault();

    var generations = clamp($('#challenge_length').val(), 50, 400);
    var width = clamp($('#challenge_size').val(), 80, 200);
    var height = clamp(Math.ceil((width/4)*3), 60, 150);

    var p = {
        generations: generations,
        width: width,
        height: height,
        _token: $('#_token').val()
    };

    if ($('.captcha').length) {
        p['recaptcha_challenge_field'] = $('#recaptcha_challenge_field').val();
        p['recaptcha_response_field'] = $('#recaptcha_response_field').val();
    }

    var posting = $.post('/api/v1/challenge', p, 'json');

    posting.done(function (data) {
        window.location = '/challenge/'+data.battle;
    });

    posting.fail(function (data) {
        alert(data.responseJSON.error)
    });
}

function doEdit(event) {
    event.preventDefault();

    var commander = $('#edit_army_commander').val();
    var id = $('#army_id').val();

    var posting = $.post('/api/v1/army/'+id, {
        commander: commander,
        active: 1,
        _token: $('#_token').val()
    }, 'json');

    posting.done(function (data) {
        window.location.reload();
    });

    posting.fail(function (data) {
        $('#edit_errors').html('<div class="alert alert-warning alert-dismissible" role="alert">'+
            '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+
            data.responseJSON.error + '</div>');
    });
}

function doDisband(event) {
    event.preventDefault();

    if (confirm('Are you sure you want to disband this army? It cannot be undone.')) {
        var commander = $('#commander_current').val();
        var id = $('#army_id').val();

        var posting = $.post('/api/v1/army/' + id, {
            commander: commander,
            _token: $('#_token').val()
        }, 'json');

        posting.done(function (data) {
            window.location.reload();
        });

        posting.fail(function (data) {
            $('#edit_errors').html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+
                data.responseJSON.error + '</div>');
        });
    }
}