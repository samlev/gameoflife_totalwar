/**
 * Created by sam on 21/10/14.
 */

$(document).ready(function () {
    board = inflate_board(board, width, height);
    my_board = inflate_board(my_board, width, height);
    board = merge_board(board, my_board);

    GOL.init();
    count = countAlive(board);

    if (count.challenger > 0) {
        $('#strength').html(count.challenger);
    } else {
        $('#strength').html(count.defender);
    }
    $('#civilians').html(count.civilian);

    if (start_x == 0) {
        fogOfWar(end_x, width, height);
    } else {
        fogOfWar(0, start_x, height);
    }

    check_ready();
});