/**
 * Created by sam on 21/10/14.
 */

GOL.handlers = {
    mouseDown: false,
    addLife: true,
    lastX: 0,
    lastY: 0,

    /**
     *
    */
    canvasMouseDown: function (event) {
        var position = GOL.helpers.mousePosition(event);
        var y = position[1], x = position[0];

        this.addLife = true;

        if (y >= 0 && y < height) {
            if (x >= start_x && x < end_x) {
                if (board[y][x] == live) {
                    this.addLife = false;
                    board[y][x] = 0;
                    strength ++;
                    $('#strength').html(strength);
                } else if (board[y][x] == 0) {
                    if (strength > 0) {
                        board[y][x] = live;
                        strength--;
                        $('#strength').html(strength);
                    }
                }
                GOL.canvas.drawCell(x, y);
                GOL.handlers.lastX = x;
                GOL.handlers.lastY = y;
                GOL.handlers.mouseDown = true;
            }
        }
    },

    /**
     *
     */
    canvasMouseUp: function () {
        GOL.handlers.mouseDown = false;
    },

    /**
     *
     */
    canvasMouseMove: function (event) {
        if (GOL.handlers.mouseDown) {
            var position = GOL.helpers.mousePosition(event);
            var y = position[1], x = position[0];
            if (y >= 0 && y < height) {
                if (x >= start_x && x < end_x) {
                    if (board[y][x] == live && this.addLife === false) {
                        board[y][x] = 0;
                        strength ++;
                        $('#strength').html(strength);
                    } else if (board[y][x] == 0 && this.addLife) {
                        if (strength > 0) {
                            board[y][x] = live;
                            strength--;
                            $('#strength').html(strength);
                        }
                    }
                    GOL.canvas.drawCell(x, y);
                    GOL.handlers.lastX = x;
                    GOL.handlers.lastY = y;
                }
            }
        }
    }
};

GOL.canvas.attachHandlers = function () {
    GOL.helpers.registerEvent(this.canvas, 'mousedown', GOL.handlers.canvasMouseDown, false);
    GOL.helpers.registerEvent(document, 'mouseup', GOL.handlers.canvasMouseUp, false);
    GOL.helpers.registerEvent(this.canvas, 'mousemove', GOL.handlers.canvasMouseMove, false);
};


$(document).ready(function () {
    board = inflate_board(board, width, height);

    GOL.init();
    var count = countAlive(board);
    $('#civilians').html(count.civilian);
    GOL.canvas.attachHandlers();

    if (start_x == 0) {
        fogOfWar(end_x, width, height);
    } else {
        fogOfWar(0, start_x, height);
    }

    $('#board_ready').click(function () {
        $(this).attr('disabled', 'disabled');

        var alive = countAlive(board);

        if ((live == 2 && alive.challenger > 0) || (live == 3 && alive.defender > 0)) {
            var b = deflate_board(board, live);

            var pl = placements(b);

            var posting = $.post('/api/v1/challenge/'+battle+'/place', {
                placements: JSON.stringify(pl),
                _token: $('#_token').val()
            }, 'json');

            posting.done(function (data) {
                window.location.reload();
            });

            posting.fail(function (data) {
                alert(data.responseJSON.error);
                $(this).removeAttr('disabled');
            });
        }
    });

    // why is this even needed? Bloody firefox.
    $('#board_ready').removeAttr('disabled');
});