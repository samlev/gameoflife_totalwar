/**
 * Created by sam on 15/10/14.
 */

var dps_challenger = [];
var dps_civilian = [];
var dps_defender = [];
var cht;

function generation(g) {
    var count = countAlive(board);

    if (count.challenger == 0 || count.defender == 0) {
        if (count.challenger == 0) {
            displayWinner('defender');
        } else {
            displayWinner('challenger');
        }
    } else if (g > 0) {
        var b = [];

        // deep clone of the existing board
        for (var y = 0; y < height; y ++) {
            b[y] = [];
            for (var x = 0; x < width; x++) {
                b[y][x] = board[y][x];
            }
        }

        for (var y = 0; y < height; y ++) {
            for (var x = 0; x < width; x ++) {
                board[y][x] = alive(b, y, x);
                if (board[y][x] != b[y][x]) {
                    GOL.canvas.drawCell(x, y);
                }
            }
        }

        g = g - 1;
        setTimeout(function () {
            generation(g);
        }, 200);
    } else {
        if (count.challenger > count.defender) {
            displayWinner('challenger');
        } else {
            displayWinner('defender');
        }
    }

    count = countAlive(board);

    $('#challengers').html(count.challenger);
    $('#civilians').html(count.civilian);
    $('#defenders').html(count.defender);
    $('#bb-at').animate({'width': ((count.challenger / count.total) * 100) + '%'}, 200);
    $('#bb-ci').animate({'width': ((count.civilian / count.total) * 100) + '%'}, 200);
    $('#bb-de').animate({'width': ((count.defender / count.total) * 100) + '%'}, 200);

    if (cht !== undefined) {
        dps_challenger.push({x: (generations - g), y: count.challenger});
        dps_civilian.push({x: (generations - g), y: count.civilian});
        dps_defender.push({x: (generations - g), y: count.defender});
        cht.render();
    }

    if (g != undefined) {
        $('#generation').html(g);
    }
}

function displayWinner(winner) {
    $('#board').append('<div id="winner">The '+winner+' is victorious!</div>');
}

function alive(b, y, x) {
    var neighbors = 0;
    var neighbors_2 = 0;
    var neighbors_3 = 0;

    y = parseInt(y);
    x = parseInt(x);

    for (var i = clamp(y-1, 0, y); i <= clamp(y+1, y, b.length-1); i ++) {
        for (var j = clamp(x-1, 0, x); j <= clamp(x+1, x, b[y].length-1); j ++) {
            if (i == y && j == x) {
                continue;
            }

            if (b[i][j] > 0) {
                neighbors ++;
                if (b[i][j] == 2) {
                    neighbors_2 ++;
                } else if (b[i][j] == 3) {
                    neighbors_3 ++;
                }
            }
        }
    }

    if (b[y][x] != 0) {
        if (neighbors > 3 || neighbors < 2) {
            return 0;
        }
    } else {
        if (neighbors == 3) {
            if (neighbors_2 >= 2) {
                return 2;
            } else if (neighbors_3 >= 2) {
                return 3;
            }

            return 1;
        }
    }

    return b[y][x];
}

$(document).ready(function () {
    board = inflate_board(board, width, height);
    GOL.init();

    var count = countAlive(board);
    $('#challengers').html(count.challenger);
    $('#civilians').html(count.civilian);
    $('#defenders').html(count.defender);
    $('#generation').html(generations);
    $('#bb-at').width(((count.challenger / count.total) * 100) + '%');
    $('#bb-ci').width(((count.civilian / count.total) * 100) + '%');
    $('#bb-de').width(((count.defender / count.total) * 100) + '%');

    if (window['CanvasJS'] !== undefined) {
        dps_challenger.push({x: 0, y: count.challenger});
        dps_civilian.push({x: 0, y: count.civilian});
        dps_defender.push({x: 0, y: count.defender});

        var chartOptions = {
            title: {
                text: 'Battle Stats!'
            },
            axisX: {
                minimum: 0,
                maximum: generations + (generations / 10)
            },
            axisY: {
                title: 'Troops',
                minimum: 0
            },
            toolTip: {
                shared: "true"
            },
            data: [
                {
                    showInLegend: true,
                    markerSize: 0,
                    type: 'spline',
                    color: '#660000',
                    name: 'Attacker',
                    dataPoints: dps_challenger
                },
                {
                    type: 'spline',
                    showInLegend: true,
                    markerSize: 0,
                    color: '#666600',
                    name: 'Civilian',
                    dataPoints: dps_civilian
                },
                {
                    type: 'spline',
                    showInLegend: true,
                    markerSize: 0,
                    color: '#000066',
                    name: 'Defender',
                    dataPoints: dps_defender
                }
            ]
        };

        cht = new CanvasJS.Chart('battlechart', chartOptions);
        cht.render();
    }

    setTimeout(function () {
        generation(generations);
    }, 1000);
});