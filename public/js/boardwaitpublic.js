/**
 * Created by sam on 21/10/14.
 */

$(document).ready(function () {
    board = inflate_board(board, width, height);
    GOL.init();
    count = countAlive(board);
    $('#civilians').html(count.civilian);

    fogOfWar(0, width, height);

    check_ready();

    $('#challenge_accept').click(function () {
        $(this).attr('disabled', 'disabled');

        var p = {
            _token: $('#_token').val()
        };

        if ($('.captcha').length) {
            p['recaptcha_challenge_field'] = $('#recaptcha_challenge_field').val();
            p['recaptcha_response_field'] = $('#recaptcha_response_field').val();
        }

        var posting = $.post('/api/v1/challenge/'+battle+'/accept', p, 'json');

        posting.done(function (data) {
            window.location.reload();
        });

        posting.fail(function (data) {
            alert(data.responseJSON.error);
            $(this).removeAttr('disabled');
        });
    });
});